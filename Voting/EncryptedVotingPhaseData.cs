﻿using Contract;
using SystemAdapters.Runtime.Serialization;
using SystemAdapters.Security.Cryptography;

namespace Voting
{
    public class EncryptedVotingPhaseData
    {
        private readonly byte[] cipherText;
        private readonly IRsaAdapterFactory rsaAdapterFactory;
        private readonly IBinarySerializer binarySerializer;

        public EncryptedVotingPhaseData(byte[] cipherText)
            : this(cipherText, new RsaAdapterFactory(), new BinarySerializer())
        {
        }

        internal EncryptedVotingPhaseData(byte[] cipherText, IRsaAdapterFactory rsaAdapterFactory, IBinarySerializer binarySerializer)
        {
            this.cipherText = cipherText;
            this.rsaAdapterFactory = rsaAdapterFactory;
            this.binarySerializer = binarySerializer;
        }

        public VotingPhaseData Decrypt(AsymmetricKey privateKey)
        {
            var rsaAdapter = rsaAdapterFactory.Create(privateKey);
            var plainText = rsaAdapter.Decrypt(cipherText);
            var votingPhaseData = binarySerializer.Deserialize<VotingPhaseData>(plainText);
            return votingPhaseData;
        }
    }
}
