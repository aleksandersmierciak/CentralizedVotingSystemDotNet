﻿using System;
using Contract;
using SystemAdapters.Runtime.Serialization;
using SystemAdapters.Security.Cryptography;

namespace Voting
{
    public class SignedAuthenticationRequest
    {
        private readonly byte[] data;
        private readonly byte[] signature;
        private readonly IRsaAdapterFactory rsaAdapterFactory;
        private readonly IBinarySerializer binarySerializer;

        public SignedAuthenticationRequest(byte[] data, byte[] signature)
            : this(data, signature, new RsaAdapterFactory(), new BinarySerializer())
        {
        }

        internal SignedAuthenticationRequest(byte[] data, byte[] signature, IRsaAdapterFactory rsaAdapterFactory, IBinarySerializer binarySerializer)
        {
            this.data = data;
            this.signature = signature;
            this.rsaAdapterFactory = rsaAdapterFactory;
            this.binarySerializer = binarySerializer;
        }

        public EncryptedSignedAuthenticationRequest Encrypt(AsymmetricKey publicKey)
        {
            var rsaAdapter = rsaAdapterFactory.Create(publicKey);
            var plainText = binarySerializer.Serialize(this);
            var cipherText = rsaAdapter.Encrypt(plainText);
            return new EncryptedSignedAuthenticationRequest(cipherText);
        }

        public AuthenticationRequest Verify(AsymmetricKey publicKey)
        {
            var rsaAdapter = rsaAdapterFactory.Create(publicKey);
            var outcome = rsaAdapter.VerifySignature(data, signature);
            if (outcome)
            {
                var blindedToken = binarySerializer.Deserialize<BlindedToken>(data);
                return new AuthenticationRequest(blindedToken);
            }
            else
            {
                throw new ArgumentException("The signature is not verifiable using the public key provided.");
            }
        }
    }
}