﻿using Contract;
using Cryptography;

namespace Voting
{
    public class Token
    {
        private readonly IRsaBlindSignatureProviderFactory rsaBlindSignatureProviderFactory;

        public Token (byte[] data)
            : this(data, new RsaBlindSignatureProviderFactory())
        {
        }

        internal Token(byte[] data, IRsaBlindSignatureProviderFactory rsaBlindSignatureProviderFactory)
        {
            this.rsaBlindSignatureProviderFactory = rsaBlindSignatureProviderFactory;
            this.Data = data;
        }

        internal byte[] Data { get; }

        public BlindedToken Blind(AsymmetricKey privateKey)
        {
            var rsaBlindSignatureProvider = rsaBlindSignatureProviderFactory.Create(privateKey);
            var blindedData = rsaBlindSignatureProvider.PrepareForBlindSignature(this.Data);
            return new BlindedToken(blindedData);
        }
    }
}
