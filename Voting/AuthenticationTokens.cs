﻿using Contract;
using SystemAdapters.Security.Cryptography;

namespace Voting
{
    public class AuthenticationTokens
    {
        private readonly Token token;
        private readonly SignedBlindedToken signedBlindedToken;
        private readonly IRsaAdapterFactory rsaAdapterFactory;

        public AuthenticationTokens(Token token, SignedBlindedToken signedBlindedToken)
            : this(token, signedBlindedToken, new RsaAdapterFactory())
        {
        }

        internal AuthenticationTokens(Token token, SignedBlindedToken signedBlindedToken, IRsaAdapterFactory rsaAdapterFactory)
        {
            this.token = token;
            this.signedBlindedToken = signedBlindedToken;
            this.rsaAdapterFactory = rsaAdapterFactory;
        }

        public bool Verify(AsymmetricKey publicKey)
        {
            var rsaAdapter = rsaAdapterFactory.Create(publicKey);
            return rsaAdapter.VerifySignature(token.Data, signedBlindedToken.Signature);
        }
    }
}
