﻿using System.Runtime.Serialization;
using Contract;
using SystemAdapters.Security.Cryptography;

namespace Voting
{
    [DataContract]
    public class BlindedToken
    {
        [DataMember]
        private readonly byte[] data;
        private readonly IRsaAdapterFactory rsaAdapterFactory;

        public BlindedToken(byte[] data)
            : this(data, new RsaAdapterFactory())
        {
        }

        internal BlindedToken(byte[] data, IRsaAdapterFactory rsaAdapterFactory)
        {
            this.data = data;
            this.rsaAdapterFactory = rsaAdapterFactory;
        }

        public SignedBlindedToken Sign(AsymmetricKey publicKey)
        {
            var rsaAdapter = rsaAdapterFactory.Create(publicKey);
            var signedBlindedTokenData = rsaAdapter.Sign(data);
            return new SignedBlindedToken(signedBlindedTokenData);
        }
    }
}