﻿namespace Voting
{
    public class SignedBlindedToken
    {
        public SignedBlindedToken(byte[] signature)
        {
            Signature = signature;
        }

        internal byte[] Signature { get; }
    }
}