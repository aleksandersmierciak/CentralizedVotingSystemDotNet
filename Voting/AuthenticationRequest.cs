﻿using Contract;
using SystemAdapters.Runtime.Serialization;
using SystemAdapters.Security.Cryptography;

namespace Voting
{
    public class AuthenticationRequest
    {
        private readonly BlindedToken blindedToken;
        private readonly IRsaAdapterFactory rsaAdapterFactory;
        private readonly IBinarySerializer binarySerializer;

        public AuthenticationRequest(BlindedToken blindedToken)
            : this(blindedToken, new RsaAdapterFactory(), new BinarySerializer())
        {
        }

        internal AuthenticationRequest(BlindedToken blindedToken, IRsaAdapterFactory rsaAdapterFactory, IBinarySerializer binarySerializer)
        {
            this.blindedToken = blindedToken;
            this.rsaAdapterFactory = rsaAdapterFactory;
            this.binarySerializer = binarySerializer;
        }

        public SignedAuthenticationRequest Sign(AsymmetricKey privateKey)
        {
            var rsaAdapter = rsaAdapterFactory.Create(privateKey);
            var plainText = binarySerializer.Serialize(blindedToken);
            var signature = rsaAdapter.Sign(plainText);
            return new SignedAuthenticationRequest(plainText, signature);
        }
    }
}