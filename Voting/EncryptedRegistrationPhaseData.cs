﻿using Contract;
using SystemAdapters.Runtime.Serialization;
using SystemAdapters.Security.Cryptography;

namespace Voting
{
    public class EncryptedRegistrationPhaseData
    {
        private readonly byte[] cipherText;
        private readonly IRsaAdapterFactory rsaAdapterFactory;
        private readonly IBinarySerializer binarySerializer;

        public EncryptedRegistrationPhaseData(byte[] data)
            : this(data, new RsaAdapterFactory(), new BinarySerializer())
        {
        }

        internal EncryptedRegistrationPhaseData(byte[] data, IRsaAdapterFactory rsaAdapterFactory, IBinarySerializer binarySerializer)
        {
            this.cipherText = data;
            this.rsaAdapterFactory = rsaAdapterFactory;
            this.binarySerializer = binarySerializer;
        }

        public RegistrationPhaseData Decrypt(AsymmetricKey privateKey)
        {
            var rsaAdapter = rsaAdapterFactory.Create(privateKey);
            var plainText = rsaAdapter.Decrypt(cipherText);
            var registrationPhaseData = binarySerializer.Deserialize<RegistrationPhaseData>(plainText);
            return registrationPhaseData;
        }
    }
}
