﻿using Contract;
using SystemAdapters.Runtime.Serialization;
using SystemAdapters.Security.Cryptography;

namespace Voting
{
    public class EncryptedVotingSheet
    {
        private readonly byte[] cipherText;
        private readonly IRsaAdapterFactory rsaAdapterFactory;
        private readonly IBinarySerializer binarySerializer;

        public EncryptedVotingSheet(byte[] cipherText)
            : this(cipherText, new RsaAdapterFactory(), new BinarySerializer())
        {
        }

        internal EncryptedVotingSheet(byte[] cipherText, IRsaAdapterFactory rsaAdapterFactory, IBinarySerializer binarySerializer)
        {
            this.cipherText = cipherText;
            this.rsaAdapterFactory = rsaAdapterFactory;
            this.binarySerializer = binarySerializer;
        }

        public VotingSheet Decrypt(AsymmetricKey privateKey)
        {
            var rsaAdapter = rsaAdapterFactory.Create(privateKey);
            var plainText = rsaAdapter.Decrypt(cipherText);
            var votingSheet = binarySerializer.Deserialize<VotingSheet>(plainText);
            return new VotingSheet();
        }
    }
}
