﻿using System.Runtime.Serialization;
using Contract;
using SystemAdapters.Runtime.Serialization;
using SystemAdapters.Security.Cryptography;

namespace Voting
{
    [DataContract]
    public class RegistrationPhaseData
    {
        private readonly IRsaAdapterFactory rsaAdapterFactory;
        private readonly IBinarySerializer binarySerializer;

        public RegistrationPhaseData(AuthenticationTokens registrationTokens, AuthenticationTokens trustCenterTokens)
            : this(registrationTokens, trustCenterTokens, new RsaAdapterFactory(), new BinarySerializer())
        {
        }

        internal RegistrationPhaseData(AuthenticationTokens registrationTokens, AuthenticationTokens trustCenterTokens, IRsaAdapterFactory rsaAdapterFactory, IBinarySerializer binarySerializer)
        {
            RegistrationTokens = registrationTokens;
            TrustCenterTokens = trustCenterTokens;
            this.rsaAdapterFactory = rsaAdapterFactory;
            this.binarySerializer = binarySerializer;
        }

        [DataMember]
        public AuthenticationTokens RegistrationTokens { get; }

        [DataMember]
        public AuthenticationTokens TrustCenterTokens { get; }

        public EncryptedRegistrationPhaseData Encrypt(AsymmetricKey publicKey)
        {
            var rsaAdapter = rsaAdapterFactory.Create(publicKey);
            var plainText = binarySerializer.Serialize(this);
            var cipherText = rsaAdapter.Encrypt(plainText);
            return new EncryptedRegistrationPhaseData(cipherText);
        }
    }
}
