﻿using Contract;
using SystemAdapters.Security.Cryptography;

namespace Voting
{
    public class EncryptedSignedBlindedToken
    {
        private readonly byte[] cipherText;
        private readonly IRsaAdapterFactory rsaAdapterFactory;

        public EncryptedSignedBlindedToken(byte[] cipherText)
            : this(cipherText, new RsaAdapterFactory())
        {
        }

        internal EncryptedSignedBlindedToken(byte[] cipherText, IRsaAdapterFactory rsaAdapterFactory)
        {
            this.cipherText = cipherText;
            this.rsaAdapterFactory = rsaAdapterFactory;
        }

        public SignedBlindedToken Decrypt(AsymmetricKey privateKey)
        {
            var rsaAdapter = rsaAdapterFactory.Create(privateKey);
            var signature = rsaAdapter.Decrypt(cipherText);
            return new SignedBlindedToken(signature);
        }
    }
}
