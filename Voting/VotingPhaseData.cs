﻿using Contract;
using SystemAdapters.Runtime.Serialization;
using SystemAdapters.Security.Cryptography;

namespace Voting
{
    public class VotingPhaseData
    {
        private readonly IRsaAdapterFactory rsaAdapterFactory;
        private readonly IBinarySerializer binarySerializer;

        public VotingPhaseData(FilledVotingSheet filledVotingSheet, RegistrationPhaseData registrationPhaseData)
            : this(filledVotingSheet, registrationPhaseData, new RsaAdapterFactory(), new BinarySerializer())
        {
        }

        internal VotingPhaseData(FilledVotingSheet filledVotingSheet, RegistrationPhaseData registrationPhaseData, IRsaAdapterFactory rsaAdapterFactory, IBinarySerializer binarySerializer)
        {
            FilledVotingSheet = filledVotingSheet;
            RegistrationPhaseData = registrationPhaseData;
            this.rsaAdapterFactory = rsaAdapterFactory;
            this.binarySerializer = binarySerializer;
        }

        public FilledVotingSheet FilledVotingSheet { get; }
        public RegistrationPhaseData RegistrationPhaseData { get; }

        public EncryptedVotingPhaseData Encrypt(AsymmetricKey publicKey)
        {
            var rsaAdapter = rsaAdapterFactory.Create(publicKey);
            var plainText = binarySerializer.Serialize(this);
            var cipherText = rsaAdapter.Encrypt(plainText);
            return new EncryptedVotingPhaseData(cipherText);
        }
    }
}