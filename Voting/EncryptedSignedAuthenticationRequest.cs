﻿using Contract;
using SystemAdapters.Runtime.Serialization;
using SystemAdapters.Security.Cryptography;

namespace Voting
{
    public class EncryptedSignedAuthenticationRequest
    {
        private readonly byte[] cipherText;
        private readonly IRsaAdapterFactory rsaAdapterFactory;
        private readonly IBinarySerializer binarySerializer;

        public EncryptedSignedAuthenticationRequest(byte[] data)
            : this(data, new RsaAdapterFactory(), new BinarySerializer())
        {
        }

        internal EncryptedSignedAuthenticationRequest(byte[] data, IRsaAdapterFactory rsaAdapterFactory, IBinarySerializer binarySerializer)
        {
            this.cipherText = data;
            this.rsaAdapterFactory = rsaAdapterFactory;
            this.binarySerializer = binarySerializer;
        }

        public SignedAuthenticationRequest Decrypt(AsymmetricKey privateKey)
        {
            var rsaAdapter = rsaAdapterFactory.Create(privateKey);
            var plainText = rsaAdapter.Decrypt(cipherText);
            var signedAuthenticationRequest = binarySerializer.Deserialize<SignedAuthenticationRequest>(plainText);
            return signedAuthenticationRequest;
        }
    }
}
