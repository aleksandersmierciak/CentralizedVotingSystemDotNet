﻿using System.Runtime.Serialization;
using System.Xml;

namespace SystemAdapters.Runtime.Serialization
{
    internal class DataContractSerializerAdapter : IDataContractSerializerAdapter
    {
        public void WriteObject<T>(XmlDictionaryWriter xmlDictionaryWriter, T graph)
        {
            var dataContractSerializer = new DataContractSerializer(typeof(T));
            dataContractSerializer.WriteObject(xmlDictionaryWriter, graph);
            xmlDictionaryWriter.Flush();
        }

        public T ReadObject<T>(XmlDictionaryReader xmlDictionaryReader)
        {
            var dataContractSerializer = new DataContractSerializer(typeof(T));
            return (T)dataContractSerializer.ReadObject(xmlDictionaryReader);
        }
    }
}
