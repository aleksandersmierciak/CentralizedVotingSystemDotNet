﻿using System.Xml;

namespace SystemAdapters.Runtime.Serialization
{
    public interface IDataContractSerializerAdapter
    {
        void WriteObject<T>(XmlDictionaryWriter xmlDictionaryWriter, T graph);
        T ReadObject<T>(XmlDictionaryReader xmlDictionaryReader);
    }
}