﻿using System.IO;
using System.Xml;

namespace SystemAdapters.Runtime.Serialization
{
    public class BinarySerializer : IBinarySerializer
    {
        private readonly IDataContractSerializerAdapter dataContractSerializerAdapter;

        public BinarySerializer()
            : this(new DataContractSerializerAdapter())
        {
        }

        internal BinarySerializer(IDataContractSerializerAdapter dataContractSerializerAdapter)
        {
            this.dataContractSerializerAdapter = dataContractSerializerAdapter;
        }

        public byte[] Serialize<T>(T instance)
        {
            var stream = SerializeIntoStream(instance);
            var bytes = LoadStreamToByteArray(stream);
            return bytes;
        }

        private MemoryStream SerializeIntoStream<T>(T instance)
        {
            var stream = new MemoryStream();
            var binaryWriter = XmlDictionaryWriter.CreateBinaryWriter(stream);
            dataContractSerializerAdapter.WriteObject(binaryWriter, instance);
            return stream;
        }

        private byte[] LoadStreamToByteArray(MemoryStream stream)
        {
            //stream.Position = 0;
            var streamContent = stream.ToArray();
            return streamContent;
        }

        public T Deserialize<T>(byte[] bytes)
        {
            var binaryReader = XmlDictionaryReader.CreateBinaryReader(bytes, XmlDictionaryReaderQuotas.Max);
            var instance = dataContractSerializerAdapter.ReadObject<T>(binaryReader);
            return instance;
        }
    }
}
