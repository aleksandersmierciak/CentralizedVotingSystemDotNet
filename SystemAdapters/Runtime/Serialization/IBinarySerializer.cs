﻿namespace SystemAdapters.Runtime.Serialization
{
    public interface IBinarySerializer
    {
        byte[] Serialize<T>(T instance);
        T Deserialize<T>(byte[] bytes);
    }
}