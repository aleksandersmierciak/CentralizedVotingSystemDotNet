﻿using System.Security.Cryptography;
using Contract;

namespace SystemAdapters.Security.Cryptography
{
    internal class RsaAdapter : IRsaAdapter
    {
        private AsymmetricKey asymmetricKey;

        internal RsaAdapter(AsymmetricKey asymmetricKey)
        {
            this.asymmetricKey = asymmetricKey;
        }

        public byte[] Encrypt(byte[] plainText)
        {
            using (var rsa = new RSACryptoServiceProvider())
            {
                rsa.FromXmlString(asymmetricKey.XmlString);
                return rsa.Encrypt(plainText, RSAEncryptionPadding.Pkcs1);
            }
        }

        public byte[] Decrypt(byte[] cipherText)
        {
            using (var rsa = new RSACryptoServiceProvider())
            {
                rsa.FromXmlString(asymmetricKey.XmlString);
                return rsa.Decrypt(cipherText, RSAEncryptionPadding.Pkcs1);
            }
        }

        public byte[] Sign(byte[] data)
        {
            using (var rsa = new RSACryptoServiceProvider())
            {
                rsa.FromXmlString(asymmetricKey.XmlString);
                return rsa.SignData(data, HashAlgorithmName.SHA512, RSASignaturePadding.Pkcs1);
            }
        }

        public bool VerifySignature(byte[] data, byte[] signature)
        {
            using (var rsa = new RSACryptoServiceProvider())
            {
                rsa.FromXmlString(asymmetricKey.XmlString);
                return rsa.VerifyData(data, signature, HashAlgorithmName.SHA512, RSASignaturePadding.Pkcs1);
            }
        }
    }
}
