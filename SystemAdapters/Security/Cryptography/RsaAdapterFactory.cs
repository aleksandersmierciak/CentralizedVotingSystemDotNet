﻿using Contract;

namespace SystemAdapters.Security.Cryptography
{
    public class RsaAdapterFactory : IRsaAdapterFactory
    {
        public IRsaAdapter Create(AsymmetricKey asymmetricKey)
        {
            return new RsaAdapter(asymmetricKey);
        }
    }
}
