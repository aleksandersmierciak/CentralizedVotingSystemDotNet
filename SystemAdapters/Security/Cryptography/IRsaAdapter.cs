﻿namespace SystemAdapters.Security.Cryptography
{
    public interface IRsaAdapter
    {
        byte[] Sign(byte[] data);
        bool VerifySignature(byte[] data, byte[] signature);
        byte[] Encrypt(byte[] plainText);
        byte[] Decrypt(byte[] cipherText);
    }
}
