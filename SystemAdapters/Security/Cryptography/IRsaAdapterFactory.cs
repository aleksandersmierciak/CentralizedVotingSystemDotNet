﻿using Contract;

namespace SystemAdapters.Security.Cryptography
{
    public interface IRsaAdapterFactory
    {
        IRsaAdapter Create(AsymmetricKey asymmetricKey);
    }
}
