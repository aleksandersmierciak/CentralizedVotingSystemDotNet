﻿using NUnit.Framework;

namespace Voting.Tests
{
    [TestFixture]
    internal class VotingSheetTests
    {
        [Test]
        public void FillReturnsFilledVotingSheet()
        {
            Assert.That(new VotingSheet().Fill(), Is.InstanceOf<FilledVotingSheet>());
        }
    }
}
