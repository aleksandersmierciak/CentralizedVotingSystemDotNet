﻿using Contract;
using Moq;
using NUnit.Framework;
using SystemAdapters.Runtime.Serialization;
using SystemAdapters.Security.Cryptography;

namespace Voting.Tests
{
    [TestFixture]
    internal class AuthenticationRequestTests
    {
        private Mock<IRsaAdapter> rsaAdapterMock;
        private Mock<IRsaAdapterFactory> rsaAdapterFactoryMock;
        private Mock<IBinarySerializer> binarySerializerMock;

        [SetUp]
        public void SetUp()
        {
            rsaAdapterMock = new Mock<IRsaAdapter>();
            rsaAdapterFactoryMock = new Mock<IRsaAdapterFactory>();
            rsaAdapterFactoryMock
                .Setup(mock => mock.Create(It.IsAny<AsymmetricKey>()))
                .Returns(rsaAdapterMock.Object);
            binarySerializerMock = new Mock<IBinarySerializer>();
        }

        [Test]
        public void SignCallsRsaAdapter()
        {
            // given
            var data = new byte[0];
            var authenticationRequest = CreateAuthenticationRequest(data);

            // when
            PerformSigning(authenticationRequest);

            // then
            rsaAdapterMock.Verify(
                mock => mock.Sign(data),
                Times.Once);
        }

        [Test]
        public void SignCallsBinarySerializer()
        {
            // given
            var data = new byte[0];
            var blindedToken = new BlindedToken(data);
            var authenticationRequest = CreateAuthenticationRequest(blindedToken);

            // when
            PerformSigning(authenticationRequest);

            // then
            binarySerializerMock.Verify(
                mock => mock.Serialize(blindedToken),
                Times.Once);
        }

        private AuthenticationRequest CreateAuthenticationRequest(byte[] data)
        {
            var blindedToken = new BlindedToken(data);
            return CreateAuthenticationRequest(blindedToken);
        }

        private AuthenticationRequest CreateAuthenticationRequest(BlindedToken blindedToken)
        {
            return new AuthenticationRequest(blindedToken, rsaAdapterFactoryMock.Object, binarySerializerMock.Object);
        }

        private SignedAuthenticationRequest PerformSigning(AuthenticationRequest authenticationRequest)
        {
            return PerformSigning(authenticationRequest, new AsymmetricKey(""));
        }

        private SignedAuthenticationRequest PerformSigning(AuthenticationRequest authenticationRequest, AsymmetricKey asymmetricKey)
        {
            return authenticationRequest.Sign(asymmetricKey);
        }
    }
}
