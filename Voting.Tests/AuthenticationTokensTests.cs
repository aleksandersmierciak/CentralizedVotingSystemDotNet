﻿using Contract;
using Moq;
using NUnit.Framework;
using SystemAdapters.Security.Cryptography;

namespace Voting.Tests
{
    [TestFixture]
    internal class AuthenticationTokensTests
    {
        private Mock<IRsaAdapter> rsaAdapterMock;
        private Mock<IRsaAdapterFactory> rsaAdapterFactoryMock;

        [SetUp]
        public void SetUp()
        {
            rsaAdapterMock = new Mock<IRsaAdapter>();
            rsaAdapterFactoryMock = new Mock<IRsaAdapterFactory>();
            rsaAdapterFactoryMock
                .Setup(mock => mock.Create(It.IsAny<AsymmetricKey>()))
                .Returns(rsaAdapterMock.Object);
        }

        [Test]
        public void VerifyCallsRsaAdapter()
        {
            // given
            var data = new byte[0];
            var signature = new byte[0];

            // when
            PerformSigning(data, signature);

            // then
            rsaAdapterMock.Verify(
                mock => mock.VerifySignature(data, signature),
                Times.Once);
        }

        [Test]
        public void VerifyReturnsRsaAdapterVerifySignature()
        {
            // given
            const bool expected = true;
            rsaAdapterMock
                .Setup(mock => mock.VerifySignature(It.IsAny<byte[]>(), It.IsAny<byte[]>()))
                .Returns(expected);

            // when
            var actual = PerformSigning();

            // then
            Assert.That(actual, Is.EqualTo(expected));
        }

        private bool PerformSigning()
        {
            return PerformSigning(new byte[0], new byte[0]);
        }

        private bool PerformSigning(byte[] data, byte[] signature)
        {
            return PerformVerification(data, signature, new AsymmetricKey(""));
        }

        private bool PerformVerification(byte[] data, byte[] signature, AsymmetricKey asymmetricKey)
        {
            var authenticationTokens = CreateAuthenticationTokens(data, signature);
            return authenticationTokens.Verify(asymmetricKey);
        }

        private AuthenticationTokens CreateAuthenticationTokens(byte[] data, byte[] signature)
        {
            var token = new Token(data);
            var signedBlindedToken = new SignedBlindedToken(signature);
            return new AuthenticationTokens(token, signedBlindedToken, rsaAdapterFactoryMock.Object);
        }
    }
}
