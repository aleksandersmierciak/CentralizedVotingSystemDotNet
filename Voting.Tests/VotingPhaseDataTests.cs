﻿using System;
using Contract;
using Moq;
using NUnit.Framework;
using SystemAdapters.Runtime.Serialization;
using SystemAdapters.Security.Cryptography;

namespace Voting.Tests
{
    [TestFixture]
    internal class VotingPhaseDataTests
    {
        private Mock<IRsaAdapter> rsaAdapterMock;
        private Mock<IRsaAdapterFactory> rsaAdapterFactoryMock;
        private Mock<IBinarySerializer> binarySerializerMock;

        [SetUp]
        public void SetUp()
        {
            rsaAdapterMock = new Mock<IRsaAdapter>();
            rsaAdapterFactoryMock = new Mock<IRsaAdapterFactory>();
            rsaAdapterFactoryMock
                .Setup(mock => mock.Create(It.IsAny<AsymmetricKey>()))
                .Returns(rsaAdapterMock.Object);
            binarySerializerMock = new Mock<IBinarySerializer>();
        }

        [Test]
        public void EncryptCallsRsaAdapter()
        {
            // given
            var plainText = new byte[0];

            // when
            PerformEncrypting(plainText);

            // then
            rsaAdapterMock.Verify(
                mock => mock.Encrypt(plainText),
                Times.Once);
        }

        [Test]
        public void EncryptCallsBinarySerializer()
        {
            // given
            var plainText = new byte[0];
            rsaAdapterMock
                .Setup(mock => mock.Decrypt(It.IsAny<byte[]>()))
                .Returns(plainText);

            // when
            var votingPhaseData = CreateVotingPhaseData(plainText);
            PerformEncrypting(votingPhaseData);

            // then
            binarySerializerMock.Verify(
                mock => mock.Serialize(votingPhaseData),
                Times.Once);
        }

        private EncryptedVotingPhaseData PerformEncrypting(VotingPhaseData votingPhaseData)
        {
            return PerformEncrypting(votingPhaseData, new AsymmetricKey(""));
        }

        private EncryptedVotingPhaseData PerformEncrypting(VotingPhaseData votingPhaseData, AsymmetricKey asymmetricKey)
        {
            return votingPhaseData.Encrypt(asymmetricKey);
        }

        [Test]
        public void EncryptReturnsNotNull()
        {
            // given
            var plainText = new byte[0];

            // when
            var actual = PerformEncrypting(plainText);

            // then
            Assert.That(actual, Is.Not.Null);
        }

        private EncryptedVotingPhaseData PerformEncrypting(byte[] plainText)
        {
            return PerformEncrypting(plainText, new AsymmetricKey(""));
        }

        private EncryptedVotingPhaseData PerformEncrypting(byte[] plainText, AsymmetricKey asymmetricKey)
        {
            var votingPhaseData = CreateVotingPhaseData(plainText);
            return votingPhaseData.Encrypt(asymmetricKey);
        }

        private VotingPhaseData CreateVotingPhaseData(byte[] cipherText)
        {
            return new VotingPhaseData(
                new FilledVotingSheet(),
                new RegistrationPhaseData(
                    new AuthenticationTokens(
                        new Token(
                            new byte[0]),
                        new SignedBlindedToken(
                            new byte[0])),
                    new AuthenticationTokens(
                        new Token(
                            new byte[0]),
                        new SignedBlindedToken(
                            new byte[0]))),
                rsaAdapterFactoryMock.Object,
                binarySerializerMock.Object);
        }
    }
}
