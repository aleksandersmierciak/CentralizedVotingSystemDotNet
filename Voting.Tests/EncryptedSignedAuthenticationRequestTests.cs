﻿using Contract;
using Moq;
using NUnit.Framework;
using SystemAdapters.Runtime.Serialization;
using SystemAdapters.Security.Cryptography;

namespace Voting.Tests
{
    [TestFixture]
    internal class EncryptedSignedAuthenticationRequestTests
    {
        private Mock<IRsaAdapter> rsaAdapterMock;
        private Mock<IRsaAdapterFactory> rsaAdapterFactoryMock;
        private Mock<IBinarySerializer> binarySerializerMock;

        [SetUp]
        public void SetUp()
        {
            rsaAdapterMock = new Mock<IRsaAdapter>();
            rsaAdapterFactoryMock = new Mock<IRsaAdapterFactory>();
            rsaAdapterFactoryMock
                .Setup(mock => mock.Create(It.IsAny<AsymmetricKey>()))
                .Returns(rsaAdapterMock.Object);
            binarySerializerMock = new Mock<IBinarySerializer>();
        }

        [Test]
        public void DecryptCallsRsaAdapter()
        {
            // given
            var cipherText = new byte[0];

            // when
            PerformDecrypting(cipherText);

            // then
            rsaAdapterMock.Verify(
                mock => mock.Decrypt(cipherText),
                Times.Once);
        }

        [Test]
        public void DecryptCallsBinarySerializer()
        {
            // given
            var cipherText = new byte[0];
            var plainText = new byte[0];
            rsaAdapterMock.Setup(mock => mock.Decrypt(It.IsAny<byte[]>())).Returns(plainText);

            // when
            PerformDecrypting(cipherText);

            // then
            binarySerializerMock.Verify(
                mock => mock.Deserialize<SignedAuthenticationRequest>(plainText),
                Times.Once);
        }

        private SignedAuthenticationRequest PerformDecrypting(byte[] cipherText)
        {
            return PerformDecrypting(cipherText, new AsymmetricKey(""));
        }

        private SignedAuthenticationRequest PerformDecrypting(byte[] cipherText, AsymmetricKey asymmetricKey)
        {
            var encryptedSignedAuthenticationRequest = CreateEncryptedSignedAuthenticationRequest(cipherText);
            return encryptedSignedAuthenticationRequest.Decrypt(asymmetricKey);
        }

        private EncryptedSignedAuthenticationRequest CreateEncryptedSignedAuthenticationRequest(byte[] cipherText)
        {
            return new EncryptedSignedAuthenticationRequest(cipherText, rsaAdapterFactoryMock.Object, binarySerializerMock.Object);
        }
    }
}
