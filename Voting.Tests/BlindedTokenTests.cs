﻿using Contract;
using Moq;
using NUnit.Framework;
using SystemAdapters.Security.Cryptography;

namespace Voting.Tests
{
    [TestFixture]
    internal class BlindedTokenTests
    {
        private Mock<IRsaAdapter> rsaAdapterMock;
        private Mock<IRsaAdapterFactory> rsaAdapterFactoryMock;

        [SetUp]
        public void SetUp()
        {
            rsaAdapterMock = new Mock<IRsaAdapter>();
            rsaAdapterFactoryMock = new Mock<IRsaAdapterFactory>();
            rsaAdapterFactoryMock
                .Setup(mock => mock.Create(It.IsAny<AsymmetricKey>()))
                .Returns(rsaAdapterMock.Object);
        }

        [Test]
        public void SignCallsRsaAdapter()
        {
            // given
            var data = new byte[0];

            // when
            PerformSigning(data);

            // then
            rsaAdapterMock.Verify(
                mock => mock.Sign(data),
                Times.Once);
        }

        private SignedBlindedToken PerformSigning(byte[] data)
        {
            return PerformSigning(data, new AsymmetricKey(""));
        }

        private SignedBlindedToken PerformSigning(byte[] data, AsymmetricKey asymmetricKey)
        {
            var blindedToken = CreateBlindedToken(data);
            return blindedToken.Sign(asymmetricKey);
        }

        private BlindedToken CreateBlindedToken(byte[] data)
        {
            return new BlindedToken(data, rsaAdapterFactoryMock.Object);
        }
    }
}
