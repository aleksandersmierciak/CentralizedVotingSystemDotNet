﻿using System;
using Contract;
using Moq;
using NUnit.Framework;
using SystemAdapters.Runtime.Serialization;
using SystemAdapters.Security.Cryptography;

namespace Voting.Tests
{
    [TestFixture]
    internal class SignedAuthenticationRequestTests
    {
        private Mock<IRsaAdapter> rsaAdapterMock;
        private Mock<IRsaAdapterFactory> rsaAdapterFactoryMock;
        private Mock<IBinarySerializer> binarySerializerMock;

        [SetUp]
        public void SetUp()
        {
            rsaAdapterMock = new Mock<IRsaAdapter>();
            rsaAdapterFactoryMock = new Mock<IRsaAdapterFactory>();
            rsaAdapterFactoryMock
                .Setup(mock => mock.Create(It.IsAny<AsymmetricKey>()))
                .Returns(rsaAdapterMock.Object);
            binarySerializerMock = new Mock<IBinarySerializer>();
        }

        [Test]
        public void EncryptCallsRsaAdapter()
        {
            // given
            var data = new byte[0];
            var signature = new byte[0];

            // when
            PerformEncrypting(data, signature);

            // then
            rsaAdapterMock.Verify(
                mock => mock.Encrypt(data),
                Times.Once);
        }

        private EncryptedSignedAuthenticationRequest PerformEncrypting(byte[] plainText, byte[] signature)
        {
            return PerformEncrypting(plainText, signature, new AsymmetricKey(""));
        }

        private EncryptedSignedAuthenticationRequest PerformEncrypting(byte[] plainText, byte[] signature, AsymmetricKey asymmetricKey)
        {
            var signedAuthenticationRequest = CreateSignedAuthenticationRequest(plainText, signature);
            return signedAuthenticationRequest.Encrypt(asymmetricKey);
        }

        [Test]
        public void EncryptCallsBinarySerializer()
        {
            // given
            var data = new byte[0];
            var signature = new byte[0];
            rsaAdapterMock.Setup(mock => mock.Decrypt(It.IsAny<byte[]>())).Returns(data);

            // when
            var signedAuthenticationRequest = CreateSignedAuthenticationRequest(data, signature);
            PerformEncrypting(signedAuthenticationRequest);

            // then
            binarySerializerMock.Verify(
                mock => mock.Serialize(signedAuthenticationRequest),
                Times.Once);
        }

        private void PerformEncrypting(SignedAuthenticationRequest signedAuthenticationRequest)
        {
            PerformEncrypting(signedAuthenticationRequest, new AsymmetricKey(""));
        }

        private void PerformEncrypting(SignedAuthenticationRequest signedAuthenticationRequest, AsymmetricKey asymmetricKey)
        {
            signedAuthenticationRequest.Encrypt(asymmetricKey);
        }

        [Test]
        public void VerifyThrowsArgumentExceptionIfPublicKeyCannotVerifySignature()
        {
            Assert.That(
                () => PerformVerification(),
                Throws.ArgumentException);
        }

        private AuthenticationRequest PerformVerification()
        {
            return PerformVerification(new byte[0], new byte[0], new AsymmetricKey(""));
        }

        [Test]
        public void VerifyCallsRsaAdapter()
        {
            // given
            var plainText = new byte[0];
            var signature = new byte[0];
            rsaAdapterMock
                .Setup(mock => mock.VerifySignature(It.IsAny<byte[]>(), It.IsAny<byte[]>()))
                .Returns(true);

            // when
            PerformVerification(plainText, signature);

            // then
            rsaAdapterMock.Verify(
                mock => mock.VerifySignature(plainText, signature),
                Times.Once);
        }

        [Test]
        public void VerifyCallsBinarySerializer()
        {
            // given
            var data = new byte[0];
            var signature = new byte[0];
            rsaAdapterMock
                .Setup(mock => mock.VerifySignature(It.IsAny<byte[]>(), It.IsAny<byte[]>()))
                .Returns(true);

            // when
            PerformVerification(data, signature);

            // then
            binarySerializerMock.Verify(
                mock => mock.Deserialize<BlindedToken>(data),
                Times.Once);
        }

        private AuthenticationRequest PerformVerification(byte[] plainText, byte[] signature)
        {
            return PerformVerification(plainText, signature, new AsymmetricKey(""));
        }

        private AuthenticationRequest PerformVerification(byte[] plainText, byte[] signature, AsymmetricKey asymmetricKey)
        {
            var signedAuthenticationRequest = CreateSignedAuthenticationRequest(plainText, signature);
            return signedAuthenticationRequest.Verify(asymmetricKey);
        }

        private SignedAuthenticationRequest CreateSignedAuthenticationRequest(byte[] plainText, byte[] signature)
        {
            return new SignedAuthenticationRequest(plainText, signature, rsaAdapterFactoryMock.Object, binarySerializerMock.Object);
        }
    }
}
