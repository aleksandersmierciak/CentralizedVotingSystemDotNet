﻿using Contract;
using Cryptography;
using Moq;
using NUnit.Framework;

namespace Voting.Tests
{
    [TestFixture]
    internal class TokenTests
    {
        private Mock<IRsaBlindSignatureProvider> rsaBlindSignatureProviderMock;
        private Mock<IRsaBlindSignatureProviderFactory> rsaBlindSignatureProviderFactoryMock;

        [SetUp]
        public void SetUp()
        {
            rsaBlindSignatureProviderMock = new Mock<IRsaBlindSignatureProvider>();
            rsaBlindSignatureProviderFactoryMock = new Mock<IRsaBlindSignatureProviderFactory>();
            rsaBlindSignatureProviderFactoryMock
                .Setup(mock => mock.Create(It.IsAny<AsymmetricKey>()))
                .Returns(rsaBlindSignatureProviderMock.Object);
        }

        [Test]
        public void BlindCallsRsaBlindSignatureProvider()
        {
            // given
            var data = new byte[0];

            // when
            PerformBlinding(data);

            // then
            rsaBlindSignatureProviderMock.Verify(
                mock => mock.PrepareForBlindSignature(data),
                Times.Once);
        }

        private BlindedToken PerformBlinding(byte[] data)
        {
            return PerformBlinding(data, new AsymmetricKey(""));
        }

        private BlindedToken PerformBlinding(byte[] data, AsymmetricKey asymmetricKey)
        {
            var token = CreateToken(data);
            return token.Blind(asymmetricKey);
        }

        private Token CreateToken(byte[] data)
        {
            return new Token(data, rsaBlindSignatureProviderFactoryMock.Object);
        }
    }
}
