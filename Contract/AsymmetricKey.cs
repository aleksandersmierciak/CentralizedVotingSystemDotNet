﻿namespace Contract
{
    public class AsymmetricKey
    {
        public AsymmetricKey(string xmlString)
        {
            XmlString = xmlString;
        }

        public string XmlString { get; }
    }
}
