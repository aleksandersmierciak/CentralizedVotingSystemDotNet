﻿namespace Contract
{
    public class AsymmetricKeyPair
    {
        public AsymmetricKeyPair(AsymmetricKey privateKey, AsymmetricKey publicKey)
        {
            PrivateKey = privateKey;
            PublicKey = publicKey;
        }

        public AsymmetricKey PrivateKey { get; }
        public AsymmetricKey PublicKey { get; }
    }
}
