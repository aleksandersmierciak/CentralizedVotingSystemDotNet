﻿using Contract;
using NUnit.Framework;

namespace Cryptography.Tests
{
    [TestFixture]
    internal class RsaBlindSignatureProviderTests
    {
        private IRsaBlindSignatureProvider rsaBlindSignatureProvider;

        [SetUp]
        public void SetUp()
        {
            var asymmetricKey = new AsymmetricKey("");
            rsaBlindSignatureProvider = new RsaBlindSignatureProvider(asymmetricKey);
        }

        [Test]
        public void PrepareForBlindSignatureReturnsZeroLengthByteArray()
        {
            Assert.That(
                rsaBlindSignatureProvider.PrepareForBlindSignature(new byte[0]),
                Has.Length.EqualTo(0));
        }

        [Test]
        public void BlindlySignReturnsZeroLengthByteArray()
        {
            Assert.That(
                rsaBlindSignatureProvider.BlindlySign(new byte[0]),
                Has.Length.EqualTo(0));
        }
    }
}
