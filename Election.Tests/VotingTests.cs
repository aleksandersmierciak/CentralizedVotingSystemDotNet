﻿using NUnit.Framework;
using Voting;
using VotingSystem;

namespace Election.Tests
{
    [TestFixture]
    internal class VotingTests
    {
        [Test]
        public void VotingDoesNotThrow()
        {
            var voter = CreateVoter();
            var votingSystem = CreateVotingSystem();

            Assert.That(() => PerformVoting(voter, votingSystem), Throws.Nothing);
        }

        private IVoter CreateVoter()
        {
            var voterFactory = new VoterFactory();
            var voter = voterFactory.Create();
            return voter;
        }

        private IVotingSystem CreateVotingSystem()
        {
            var votingSystemFactory = new VotingSystemFactory();
            var votingSystem = votingSystemFactory.Create();
            return votingSystem;
        }

        private void PerformVoting(IVoter voter, IVotingSystem votingSystem)
        {
            var registrationTokens = voter.Authenticate(votingSystem.Registration);
            var trustCenterTokens = voter.Authenticate(votingSystem.TrustCenter);
            var registrationPhaseData = new RegistrationPhaseData(registrationTokens, trustCenterTokens);

            var votingSheet = voter.ObtainVotingSheet(votingSystem.BallotBox, registrationPhaseData);
            var filledVotingSheet = votingSheet.Fill();
            voter.CastVote(votingSystem.BallotBox, filledVotingSheet);
        }
    }
}
