﻿using System.Security.Cryptography;
using System.Text;
using Contract;
using NUnit.Framework;
using SystemAdapters.Security.Cryptography;

namespace SystemAdapters.Tests.Security.Cryptography
{
    [TestFixture]
    internal class RsaAdapterTests
    {
        private readonly AsymmetricKey asymmetricKey;
        private readonly RsaAdapter rsaAdapter;

        public RsaAdapterTests()
        {
            var xmlContent = ObtainRandomRsaKeyPair();
            asymmetricKey = new AsymmetricKey(xmlContent);
            rsaAdapter = new RsaAdapter(asymmetricKey);
        }

        private string ObtainRandomRsaKeyPair()
        {
            using (var rsa = new RSACryptoServiceProvider())
            {
                return rsa.ToXmlString(true);
            }
        }

        [Test]
        public void EncryptionAndDecryptionAreInverseFunctions()
        {
            // given
            var plainText = Encoding.UTF8.GetBytes("This is some input data");
            var cipherText = rsaAdapter.Encrypt(plainText);

            // when
            var outcome = rsaAdapter.Decrypt(cipherText);

            // then
            Assert.That(outcome, Is.EqualTo(plainText));
        }

        [Test]
        public void SigningAndVerifyingAreInverseFunctions()
        {
            // given
            var data = Encoding.UTF8.GetBytes("This is some input data");
            var signature = rsaAdapter.Sign(data);

            // when
            var outcome = rsaAdapter.VerifySignature(data, signature);

            // then
            Assert.That(outcome, Is.True);
        }
    }
}
