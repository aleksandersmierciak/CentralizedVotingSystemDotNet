﻿using NUnit.Framework;
using SystemAdapters.Runtime.Serialization;

namespace SystemAdapters.Tests.Runtime.Serialization
{
    [TestFixture]
    internal class BinarySerializerTests
    {
        [Test]
        public void SerializationIsReversible()
        {
            // given
            var initial = new SerializationTestClass("Something", 3);

            // when
            var binarySerializer = new BinarySerializer();
            var serialized = binarySerializer.Serialize(initial);
            var deserialized = binarySerializer.Deserialize<SerializationTestClass>(serialized);

            // then
            Assert.That(deserialized.Equals(initial));
        }
    }
}
