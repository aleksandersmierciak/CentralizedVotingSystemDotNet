﻿using System;
using System.Runtime.Serialization;

namespace SystemAdapters.Tests.Runtime.Serialization
{
    [DataContract]
    public class SerializationTestClass : IEquatable<SerializationTestClass>
    {
        [DataMember]
        private readonly string first;

        [DataMember]
        private readonly int second;

        public SerializationTestClass(string first, int second)
        {
            this.first = first;
            this.second = second;
        }

        public bool Equals(SerializationTestClass other)
        {
            return this.first == other.first
                && this.second == other.second;
        }
    }
}
