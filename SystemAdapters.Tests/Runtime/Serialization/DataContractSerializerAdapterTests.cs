﻿using System.IO;
using System.Xml;
using NUnit.Framework;
using SystemAdapters.Runtime.Serialization;

namespace SystemAdapters.Tests.Runtime.Serialization
{
    [TestFixture]
    internal class DataContractSerializerAdapterTests
    {
        private IDataContractSerializerAdapter dataContractSerializerAdapter;

        [SetUp]
        public void SetUp()
        {
            dataContractSerializerAdapter = new DataContractSerializerAdapter();
        }

        [Test]
        public void SerializationIsReversible()
        {
            // given
            var initial = new SerializationTestClass("Something", 3);

            // when
            var serialized = Serialize(initial);
            var deserialized = Deserialize(serialized);

            //then
            Assert.That(deserialized.Equals(initial));
        }

        private MemoryStream Serialize(SerializationTestClass item)
        {
            var stream = new MemoryStream();
            var binaryWriter = XmlDictionaryWriter.CreateBinaryWriter(stream);
            dataContractSerializerAdapter.WriteObject(binaryWriter, item);
            return stream;
        }

        private SerializationTestClass Deserialize(MemoryStream stream)
        {
            stream.Position = 0;
            var binaryReader = XmlDictionaryReader.CreateBinaryReader(stream, XmlDictionaryReaderQuotas.Max);
            var deserialized = dataContractSerializerAdapter.ReadObject<SerializationTestClass>(binaryReader);
            return deserialized;
        }
    }
}
