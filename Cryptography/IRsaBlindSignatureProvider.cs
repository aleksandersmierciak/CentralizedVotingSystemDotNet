﻿namespace Cryptography
{
    public interface IRsaBlindSignatureProvider
    {
        byte[] PrepareForBlindSignature(byte[] data);
        byte[] BlindlySign(byte[] data);
    }
}