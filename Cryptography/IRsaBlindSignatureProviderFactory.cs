﻿using Contract;

namespace Cryptography
{
    public interface IRsaBlindSignatureProviderFactory
    {
        IRsaBlindSignatureProvider Create(AsymmetricKey asymmetricKey);
    }
}