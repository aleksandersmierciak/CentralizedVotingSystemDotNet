﻿using System;
using Contract;

namespace Cryptography
{
    internal class RsaBlindSignatureProvider : IRsaBlindSignatureProvider
    {
        private readonly AsymmetricKey asymmetricKey;

        public RsaBlindSignatureProvider(AsymmetricKey asymmetricKey)
        {
            this.asymmetricKey = asymmetricKey;
        }

        public byte[] PrepareForBlindSignature(byte[] data)
        {
            return new byte[0];
        }

        public byte[] BlindlySign(byte[] data)
        {
            return new byte[0];
        }
    }
}
