﻿using Contract;

namespace Cryptography
{
    public class RsaBlindSignatureProviderFactory : IRsaBlindSignatureProviderFactory
    {
        public IRsaBlindSignatureProvider Create(AsymmetricKey asymmetricKey)
        {
            return new RsaBlindSignatureProvider(asymmetricKey);
        }
    }
}
