﻿using NUnit.Framework;
using Voting;

namespace VotingSystem.Tests
{
    [TestFixture]
    internal class VoterTests
    {
        private readonly RegistrationFactory registrationFactory = new RegistrationFactory();
        private readonly TrustCenterFactory trustCenterFactory = new TrustCenterFactory();
        private readonly BallotBoxFactory ballotBoxFactory = new BallotBoxFactory();
        private readonly VoterFactory voterFactory = new VoterFactory();

        [Test]
        public void ConstructorDoesNotThrow()
        {
            Assert.That(() => voterFactory.Create(), Throws.Nothing);
        }

        [Test]
        public void RegistrationAuthenticateDoesNotThrow()
        {
            Assert.That(
                () => PerformRegistrationAuthenticate(),
                Throws.Nothing);
        }

        private AuthenticationTokens PerformRegistrationAuthenticate()
        {
            var voter = voterFactory.Create();
            var registration = registrationFactory.Create();
            return voter.Authenticate(registration);
        }

        [Test]
        public void TrustCenterAuthenticateDoesNotThrow()
        {
            Assert.That(
                () => PerformTrustCenterAuthenticate(),
                Throws.Nothing);
        }

        private AuthenticationTokens PerformTrustCenterAuthenticate()
        {
            var voter = voterFactory.Create();
            var trustCenter = trustCenterFactory.Create();
            return voter.Authenticate(trustCenter);
        }

        [Test]
        public void ObtainVotingSheetDoesNotThrow()
        {
            Assert.That(
                () => PerformObtainVotingSheet(),
                Throws.Nothing);
        }

        private VotingSheet PerformObtainVotingSheet()
        {
            var voter = voterFactory.Create();
            var ballotBox = ballotBoxFactory.Create();
            var registrationPhaseData = new RegistrationPhaseData(
                new AuthenticationTokens(
                    new Token(new byte[0]),
                    new SignedBlindedToken(new byte[0])),
                new AuthenticationTokens(
                    new Token(new byte[0]),
                    new SignedBlindedToken(new byte[0])));
            return voter.ObtainVotingSheet(ballotBox, registrationPhaseData);
        }

        [Test]
        public void CastVoteDoesNotThrow()
        {
            Assert.That(
                () => PerformCastVote(),
                Throws.Nothing);
        }

        private void PerformCastVote()
        {
            var voter = voterFactory.Create();
            var ballotBox = ballotBoxFactory.Create();
            var filledVotingSheet = new FilledVotingSheet();
            voter.CastVote(ballotBox, filledVotingSheet);
        }
    }
}
