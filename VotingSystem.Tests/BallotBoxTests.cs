﻿using Contract;
using NUnit.Framework;
using Voting;

namespace VotingSystem.Tests
{
    [TestFixture]
    internal class BallotBoxTests
    {
        [Test]
        public void ConstructorDoesNotThrow()
        {
            Assert.That(() => CreateBallotBox(), Throws.Nothing);
        }

        [Test]
        public void ObtainVotingSheetDoesNotThrow()
        {
            Assert.That(
                () => PerformObtainVotingSheet(),
                Throws.Nothing);
        }

        [Test]
        public void CastVoteDoesNotThrow()
        {
            Assert.That(
                () => PerformCastVote(),
                Throws.Nothing);
        }

        private EncryptedVotingSheet PerformObtainVotingSheet()
        {
            var ballotBox = CreateBallotBox();
            return ballotBox.ObtainVotingSheet(
                    new EncryptedRegistrationPhaseData(new byte[0]));
        }

        private void PerformCastVote()
        {
            var ballotBox = CreateBallotBox();
            ballotBox.CastVote(
                    new EncryptedVotingPhaseData(new byte[0]));
        }

        private IBallotBox CreateBallotBox()
        {
            return new BallotBox(
                new AsymmetricKeyPair(
                   new AsymmetricKey(""),
                   new AsymmetricKey("")));
        }
    }
}
