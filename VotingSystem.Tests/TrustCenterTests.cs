﻿using NUnit.Framework;
using Voting;

namespace VotingSystem.Tests
{
    [TestFixture]
    internal class TrustCenterTests
    {
        private readonly TrustCenterFactory trustCenterFactory = new TrustCenterFactory();

        [Test]
        public void ConstructorDoesNotThrow()
        {
            Assert.That(() => trustCenterFactory.Create(), Throws.Nothing);
        }

        [Test]
        public void AuthenticateDoesNotThrow()
        {
            Assert.That(
                () => PerformAuthenticate(),
                Throws.Nothing);
        }

        private EncryptedSignedBlindedToken PerformAuthenticate()
        {
            var trustCenter = trustCenterFactory.Create();
            return trustCenter.Authenticate(
                    new EncryptedSignedAuthenticationRequest(new byte[0]));
        }
    }
}
