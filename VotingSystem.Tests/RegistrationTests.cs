﻿using NUnit.Framework;
using Voting;

namespace VotingSystem.Tests
{
    [TestFixture]
    internal class RegistrationTests
    {
        private readonly RegistrationFactory registrationFactory = new RegistrationFactory();

        [Test]
        public void ConstructorDoesNotThrow()
        {
            Assert.That(() => registrationFactory.Create(), Throws.Nothing);
        }

        [Test]
        public void AuthenticateDoesNotThrow()
        {
            Assert.That(
                () => PerformAuthenticate(),
                Throws.Nothing);
        }

        private EncryptedSignedBlindedToken PerformAuthenticate()
        {
            var registration = registrationFactory.Create();
            return registration.Authenticate(
                    new EncryptedSignedAuthenticationRequest(new byte[0]));
        }
    }
}
