﻿using Voting;

namespace VotingSystem
{
    public interface IRegistration
    {
        EncryptedSignedBlindedToken Authenticate(EncryptedSignedAuthenticationRequest encryptedSignedAuthenticationRequest);
    }
}
