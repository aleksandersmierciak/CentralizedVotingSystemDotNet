﻿using Contract;

namespace VotingSystem
{
    public class BallotBoxFactory
    {
        public IBallotBox Create()
        {
            return new BallotBox(new AsymmetricKeyPair(new AsymmetricKey(""), new AsymmetricKey("")));
        }
    }
}
