﻿using Contract;

namespace VotingSystem
{
    public class VoterFactory
    {
        public IVoter Create()
        {
            return new Voter(new AsymmetricKeyPair(new AsymmetricKey(""), new AsymmetricKey("")));
        }
    }
}
