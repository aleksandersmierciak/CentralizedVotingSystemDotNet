﻿using Contract;
using Voting;

namespace VotingSystem
{
    internal class Voter : IVoter
    {
        private readonly AsymmetricKeyPair asymmetricKeyPair;

        public Voter(AsymmetricKeyPair asymmetricKeyPair)
        {
            this.asymmetricKeyPair = asymmetricKeyPair;
        }

        public AuthenticationTokens Authenticate(IRegistration registration)
        {
            return new AuthenticationTokens(new Token(new byte[0]), new SignedBlindedToken(new byte[0]));
        }

        public AuthenticationTokens Authenticate(ITrustCenter trustCenter)
        {
            return new AuthenticationTokens(new Token(new byte[0]), new SignedBlindedToken(new byte[0]));
        }

        public VotingSheet ObtainVotingSheet(IBallotBox ballotBox, RegistrationPhaseData AuthenticationTokens)
        {
            return new VotingSheet();
        }

        public void CastVote(IBallotBox ballotBox, FilledVotingSheet filledVotingSheet)
        {
        }
    }
}
