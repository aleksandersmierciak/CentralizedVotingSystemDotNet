﻿using Voting;

namespace VotingSystem
{
    public interface IVoter
    {
        AuthenticationTokens Authenticate(IRegistration registration);
        AuthenticationTokens Authenticate(ITrustCenter trustCenter);
        VotingSheet ObtainVotingSheet(IBallotBox ballotBox, RegistrationPhaseData AuthenticationTokens);
        void CastVote(IBallotBox ballotBox, FilledVotingSheet filledVotingSheet);
    }
}