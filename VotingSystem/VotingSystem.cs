﻿namespace VotingSystem
{
    internal class VotingSystem : IVotingSystem
    {
        public VotingSystem(IRegistration registration, ITrustCenter trustCenter, IBallotBox ballotBox)
        {
            Registration = registration;
            TrustCenter = trustCenter;
            BallotBox = ballotBox;
        }

        public IBallotBox BallotBox { get; }
        public IRegistration Registration { get; }
        public ITrustCenter TrustCenter { get; }
    }
}
