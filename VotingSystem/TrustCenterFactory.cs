﻿using Contract;

namespace VotingSystem
{
    public class TrustCenterFactory
    {
        public ITrustCenter Create()
        {
            return new TrustCenter(new AsymmetricKeyPair(new AsymmetricKey(""), new AsymmetricKey("")));
        }
    }
}
