﻿using Contract;

namespace VotingSystem
{
    public class RegistrationFactory
    {
        public IRegistration Create()
        {
            return new Registration(new AsymmetricKeyPair(new AsymmetricKey(""), new AsymmetricKey("")));
        }
    }
}
