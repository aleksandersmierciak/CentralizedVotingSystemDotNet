﻿namespace VotingSystem
{
    public class VotingSystemFactory
    {
        public IVotingSystem Create()
        {
            var registration = CreateRegistration();
            var trustCenter = CreateTrustCenter();
            var ballotBox = CreateBallotBox();
            return new VotingSystem(registration, trustCenter, ballotBox);
        }

        private IRegistration CreateRegistration()
        {
            var registrationFactory = new RegistrationFactory();
            var registration = registrationFactory.Create();
            return registration;
        }

        private ITrustCenter CreateTrustCenter()
        {
            var trustCenterFactory = new TrustCenterFactory();
            var trustCenter = trustCenterFactory.Create();
            return trustCenter;
        }

        private IBallotBox CreateBallotBox()
        {
            var ballotBoxFactory = new BallotBoxFactory();
            var ballotBox = ballotBoxFactory.Create();
            return ballotBox;
        }
    }
}
