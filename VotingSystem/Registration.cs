﻿using Contract;
using Voting;

namespace VotingSystem
{
    public class Registration : IRegistration
    {
        private readonly AsymmetricKeyPair asymmetricKeyPair;

        public Registration(AsymmetricKeyPair asymmetricKeyPair)
        {
            this.asymmetricKeyPair = asymmetricKeyPair;
        }

        public EncryptedSignedBlindedToken Authenticate(EncryptedSignedAuthenticationRequest encryptedSignedAuthenticationRequest)
        {
            return new EncryptedSignedBlindedToken(new byte[0]);
        }
    }
}
