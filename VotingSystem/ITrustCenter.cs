﻿using Voting;

namespace VotingSystem
{
    public interface ITrustCenter
    {
        EncryptedSignedBlindedToken Authenticate(EncryptedSignedAuthenticationRequest encryptedSignedAuthenticationRequest);
    }
}
