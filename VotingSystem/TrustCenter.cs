﻿using Contract;
using Voting;

namespace VotingSystem
{
    public class TrustCenter : ITrustCenter
    {
        private readonly AsymmetricKeyPair asymmetricKeyPair;

        public TrustCenter(AsymmetricKeyPair asymmetricKeyPair)
        {
            this.asymmetricKeyPair = asymmetricKeyPair;
        }

        public EncryptedSignedBlindedToken Authenticate(EncryptedSignedAuthenticationRequest encryptedSignedAuthenticationRequest)
        {
            return new EncryptedSignedBlindedToken(new byte[0]);
        }
    }
}
