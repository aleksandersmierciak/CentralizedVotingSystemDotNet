﻿using Voting;

namespace VotingSystem
{
    public interface IBallotBox
    {
        EncryptedVotingSheet ObtainVotingSheet(EncryptedRegistrationPhaseData encryptedRegistrationPhaseData);
        void CastVote(EncryptedVotingPhaseData encryptedVotingPhaseData);
    }
}
