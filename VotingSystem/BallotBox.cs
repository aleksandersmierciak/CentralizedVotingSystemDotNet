﻿using Contract;
using Voting;

namespace VotingSystem
{
    internal class BallotBox : IBallotBox
    {
        private readonly AsymmetricKeyPair asymmetricKeyPair;

        public BallotBox(AsymmetricKeyPair asymmetricKeyPair)
        {
            this.asymmetricKeyPair = asymmetricKeyPair;
        }

        public EncryptedVotingSheet ObtainVotingSheet(EncryptedRegistrationPhaseData encryptedRegistrationPhaseData)
        {
            return new EncryptedVotingSheet(new byte[0]);
        }

        public void CastVote(EncryptedVotingPhaseData encryptedVotingPhaseData)
        {
        }
    }
}
