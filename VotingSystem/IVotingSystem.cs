﻿namespace VotingSystem
{
    public interface IVotingSystem
    {
        IRegistration Registration { get; }
        ITrustCenter TrustCenter { get; }
        IBallotBox BallotBox { get; }
    }
}